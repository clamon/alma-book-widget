<?php

//LC Group 1 Classifications - Subject Linking
//Add terms to Switch Statement 

foreach($group1 as $term) {

	switch ($term) {
    		
                case "Human anatomy":
                        array_push($subjects, "Anatomy and Physiology");
                        break;
		case "Physiology":
			array_push($subjects, "Anatomy and Physiology");
			break;
		case "Anthropology":
        		array_push($subjects, "Anthropology");
        		break;
    		case "Visual arts":
                        array_push($subjects, "Art");
			break;
		case "Architecture":
                        array_push($subjects, "Art");
                        break;
		case "Sculpture":
                        array_push($subjects, "Art");
                        break;
		case "Drawing. Design. Illustration":
                        array_push($subjects, "Art");
                        break;
		case "Painting":
                        array_push($subjects, "Art");
                        break;
		case "Print media":
                        array_push($subjects, "Art");
                        break;
		case "Decorative arts":
                        array_push($subjects, "Art");
                        break;
		case "Arts in general":
                        array_push($subjects, "Art");
                        break;
		case "Photography":
                        array_push($subjects, "Art");
                        break;
		case "Astronomy":
                        array_push($subjects, "Astronomy");
                        break;
		case "Natural history (General)":
                        array_push($subjects, "Biological Sciences");
                        break;
		case "Biology (General)":
                        array_push($subjects, "Biological Sciences");
                        break;
		case "Commerce":
                        array_push($subjects, "Business");
                        break;
                case "Chemistry":
                        array_push($subjects, "Chemistry");
                        break;
                case "Social pathology. Social and public welfare. Criminology":
			array_push($subjects, "Criminal Justice and Criminology");
			break;
		case "Commerce":
                        array_push($subjects, "Business");
                        break;
                case "Commerce":
                        array_push($subjects, "Business");
                        break;
                case "Dentistry":
                        array_push($subjects, "Dental Hygiene");
                        break;
                case "Finance":
                        array_push($subjects, "Economics and Finance");
                        break;
                case "Economic history and conditions":
                        array_push($subjects, "Economics and Finance");
                        break;
                case "Economic theory. Demography":
                        array_push($subjects, "Economics and Finance");
                        break;
                case "Public finance":
                        array_push($subjects, "Economics and Finance");
                        break;
                case "History of education":
                        array_push($subjects, "Education");
                        break;
                case "Special aspects of education":
                        array_push($subjects, "Education");
                        break;
                case "Theory and practice of education":
                        array_push($subjects, "Education");
                        break;
                case "Electrical engineering. Electronics. Nuclear engineering":
			array_push($subjects, "Engineering");
			break;
		case "Environmental technology. Sanitary engineering":
                        array_push($subjects, "Environmental Health");
                        break;
                case "Environmental sciences":
                        array_push($subjects, "Environmental Health");
                        break;
                case "Greek philology and language":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Hyperborean, Indian, and artificial languages":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
		case "Languages of Eastern Asia, Africa, Oceania":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Latin philology and language":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Medieval and modern Greek language":
                        array_push($subjects, "Foreign Language and Literature");
                        break;						
                case "Modern languages. Celtic languages":
                        array_push($subjects, "Foreign Language and Literature");
                        break;						
                case "Afrikaans literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "American literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Byzantine and modern Greek literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Canadian literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Classical literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Danish literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;						
                case "Dutch literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "English literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "Faroese literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "Flemish literature since 1830":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "French literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "German literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;							
                case "Greek literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;							
                case "Indo-Iranian philology and literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;		
                case "Italian literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Literature (General)":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Literature on music":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Medieval and modern Latin literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Modern Icelandic literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Norwegian literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Old Norse literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;			
                case "Old Icelandic and Old Norwegian":
                        array_push($subjects, "Foreign Language and Literature");
                        break;		
                case "Oriental philology and literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Portuguese literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Roman literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Scandinavian literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;
                case "Spanish literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;				
                case "Swedish literature":
                        array_push($subjects, "Foreign Language and Literature");
                        break;	
                case "West Germanic":
			array_push($subjects, "Foreign Language and Literature");
			break;
		case "Geology":
                        array_push($subjects, "Geosciences");
                        break;
                case "History (General)":
                        array_push($subjects, "History");
                        break;		
                case "History of Africa":
                        array_push($subjects, "History");
                        break;							
                case "History of Asia":
                        array_push($subjects, "History");
			break;							
                case "History of Austria. Liechtenstein. Hungary. Czechoslovakia":
                        array_push($subjects, "History");
                        break;							
                case "History of Balkan Peninsula":
                        array_push($subjects, "History");
                        break;							
                case "History of Central Europe":
                        array_push($subjects, "History");
                        break;							
                case "History of Civilization":
                        array_push($subjects, "History");
                        array_push($subjects, "International Studies");
			break;							
                case "History of Eastern Europe (General)":
                        array_push($subjects, "History");
                        break;							
                case "History of France":
                        array_push($subjects, "History");
                        break;							
                case "History of Germany":
                        array_push($subjects, "History");
                        break;							
                case "History of Great Britain":
                        array_push($subjects, "History");
                        break;							
                case "History of Greece":
                        array_push($subjects, "History");
                        break;								
                case "History of Italy":
                        array_push($subjects, "History");
                        break;								
                case "History of Low Countries. Benelux Countries":
                        array_push($subjects, "History");
                        break;								
                case "History of Netherlands (Holland)":
                        array_push($subjects, "History");
                        break;								
                case "History of Northern Europe. Scandinavia":
                        array_push($subjects, "History");
                        break;		
                case "History of Oceania (South Seas)":
                        array_push($subjects, "History");
                        break;		
                case "History of Poland":
                        array_push($subjects, "History");
                        break;		
                case "History of Portugal":
                        array_push($subjects, "History");
                        break;		
                case "History of Romanies":
                        array_push($subjects, "History");
                        break;		
                case "History of Russia. Soviet Union. Former Soviet Republics":
                        array_push($subjects, "History");
                        break;		
                case "History of Spain; History of Switzerland":
                        array_push($subjects, "History");
                        break;							
                case "History of canon law":
                        array_push($subjects, "History");
                        break;							
                case "History of education":
                        array_push($subjects, "History");
                        break;							
                case "History of scholarship and learning. The humanities":
                        array_push($subjects, "History");
                        break;							
                case "History of the Greco-Roman world":
                        array_push($subjects, "History");
                        break;		
                case "International law, see JZ and KZ":
                        array_push($subjects, "International Studies");
                        break;
                case "International relations":
                        array_push($subjects, "International Studies");
                        break;	
                case "Law in general. Comparative and uniform law. Jurisprudence":
                        array_push($subjects, "Legal Resources");
                        break;	
                case "Law of the United States":
                        array_push($subjects, "Legal Resources");
                        break;	
                case "Medicine (General)":
                        array_push($subjects, "Medicine");
                        break;	
                case "Pathology":
                        array_push($subjects, "Medicine");
                        break;							
                case "Internal medicine":
                        array_push($subjects, "Medicine");
                        break;							
                case "Surgery":
                        array_push($subjects, "Medicine");
                        break;							
                case "Ophthalmology":
                        array_push($subjects, "Medicine");
                        break;							
                case "Otorhinolaryngology":
                        array_push($subjects, "Medicine");
                        break;							
                case "Gynecology and obstetrics":
                        array_push($subjects, "Medicine");
                        break;							
                case "Pediatrics":
                        array_push($subjects, "Medicine");
                        break;			
                case "Dermatology":
                        array_push($subjects, "Medicine");
                        break;		
                case "Botanic, Thomsonian, and eclectic medicine":
                        array_push($subjects, "Medicine");
                        break;		
                case "Homeopathy":
                        array_push($subjects, "Medicine");
                        break;	
                case "Music":
                        array_push($subjects, "Music");
                        break;			
                case "Literature on music":
                        array_push($subjects, "Music");
                        break;		
                case "Instruction and study":
                        array_push($subjects, "Music");
                        break;
                case "Nursing":
                        array_push($subjects, "Nursing");
                        break;
                case "Pharmacy and materia medica":
                        array_push($subjects, "Pharmacy");
                        break;		
                case "Therapeutics. Pharmacology":
                        array_push($subjects, "Pharmacy");
                        break;					
                case "Philosophy (General)":
                        array_push($subjects, "Philosophy");
                        break;		
                case "Physics":
                        array_push($subjects, "Physics");
                        break;
                case "Political institutions and public administration":
                        array_push($subjects, "Political Science");
                        break;			
                case "Political theory. The state. Theories of the state":
                        array_push($subjects, "Political Science");
                        break;								
                case "Political institutions and public administration (Asia,":
                        array_push($subjects, "Political Science");
                        break;							
                case "Political institutions and public administration (Europe)":
                        array_push($subjects, "Political Science");
                        break;	
                case "Local government. Municipal government":
                        array_push($subjects, "Political Science");
                        break;
                case "Psychology":
                        array_push($subjects, "Psychology");
                        break;	
                case "Communities. Classes. Races":
			array_push($subjects, "Public Administration");
			break;
		case "Sociology":
                        array_push($subjects, "Sociology");
                        break;			
	}//End Switch
}//End foreach


?>
